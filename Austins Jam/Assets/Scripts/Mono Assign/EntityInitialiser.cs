﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Mathematics;
using Unity.Collections;
using TMPro;
using UnityEngine.UI;

//this script initalises the entities
//entities cannot be fully initialised with gameobjects
//normally entities are created with scripts but gameobjects offer a path to create them from the scene and visible in the scene.

public class EntityInitialiser : MonoBehaviour
{
    Unity.Mathematics.Random rand = new Unity.Mathematics.Random((uint)System.DateTime.Now.Ticks);

    public TextMeshProUGUI Score;
    public TextMeshProUGUI MaxHealth;
    public TextMeshProUGUI CurrentHealth;
    public GameObject damage;
    public GameObject defeat;
    public GameObject safe;

    EntityManager em;
    GameObjectEntity self;

    public GameObjectEntity ship;

    public bool iniGrid;
    public bool iniShip;
    public bool iniCam;
    public bool iniCamMove;

    public bool iniScore;

    private void Awake()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        self = GetComponent<GameObjectEntity>();

        int size = PersistantData.worldSize;

        if (iniGrid)
        {
            Entity e2 = em.CreateEntity(Data.tilePoses);

            Vector3[] poses = new Vector3[size * size];
            em.SetSharedComponentData(e2,new PosArray { poss = new Vector3[size*size] });

            Entity e = em.CreateEntity(Data.tileOwner);
            em.SetComponentData(e , new GridData { div =(size*size)/3, halfDiv = size/50  } );
            DynamicBuffer<GridPointsBuffer> buff = em.GetBuffer<GridPointsBuffer>(e);

            for (int i=0; i<size;i++)
            {
                for (int j = 0; j < size; j++)
                {
                    int x = j * 50;
                    int z = i * 50;

                    //float height = rand.NextFloat(24, 40);
                    //TEAM t = (TEAM) rand.NextInt(0,3);

                    float ze = (float) j / size*10;
                    float fe = (float) i/ size*10;
                    float perl = Mathf.PerlinNoise(fe,ze)*150;
                    //Debug.Log(perl);

                    float3 pos = new Vector3(x, perl, z);

                    buff.Add(new GridTile { position = pos, team = TEAM.Null });
                    poses[i * size + j] = pos;
                }
            }

            /*
            for (int o =0; o < 1; o++)
            {
                int i1 = rand.NextInt(5, size - 5);
                int j1 = rand.NextInt(5, size - 5);
                GridTile til = buff[i1 * size + j1].tile;
                til.team = TEAM.PLAYER;
                til.HeightPowerValue = 50;
                til.emitter = 1;
                buff[i1 * size + j1] = til;
            }
            
            for (int o = 0; o < 3; o++)
            {
                int i1 = rand.NextInt(5, size - 5);
                int j1 = rand.NextInt(5, size - 5);
                GridTile til = buff[i1 * size + j1].tile;
                til.team = TEAM.RED;
                til.HeightPowerValue = 50;
                til.emitter = 1;
                buff[i1 * size + j1] = til;
            }
            */

            em.SetSharedComponentData(e2, new PosArray {  poss = poses  });
        }
        if (iniShip)
        {
            Position pos = em.GetComponentData<Position>(self.Entity);
            float t = size / 2 * 50;
            pos = new Position {Value = new float3(t,80,t) };
            em.SetComponentData(self.Entity, pos);
        }
        if (iniCam)
        {
            float t = size / 2 * 50;
            Position pos = new Position { Value = new float3(t, 1100, t) };

            transform.position = new Vector3(pos.Value.x,pos.Value.y,pos.Value.z);
        }

        //Destroy(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (iniCamMove)
        {
            //float x = Input.GetAxis("Mouse X");
            //float z = Input.GetAxis("Mouse Y");
            Position pos = em.GetComponentData<Position>(ship.Entity);

            transform.position = new Vector3(pos.Value.x, transform.position.y,(pos.Value.z-300));
        }

        if (iniScore)
        {
            UserShip us = em.GetComponentData<UserShip>(ship.Entity);
            Score.text = us.score.ToString("0.0");
            MaxHealth.text = us.maxHealth.ToString("0.0");
            CurrentHealth.text = us.health.ToString("0.0");

            if (us.damage==1)
            {
                damage.SetActive(true);
            }
            else
            {
                damage.SetActive(false);
            }

            if (us.dead==1)
            {
                defeat.SetActive(true);
                damage.SetActive(false);
            }
            else
            {
                defeat.SetActive(false);
            }

            if (us.Safe>0)
            {
                safe.SetActive(true);
            }
            else
            {
                safe.SetActive(false);
            }
        }
    }

    public void Restart()
    {
        int size = PersistantData.worldSize;

        Position p = em.GetComponentData<Position>(ship.Entity);
        UserShip us = em.GetComponentData<UserShip>(ship.Entity);

        float t = size / 2 * 50;
        p = new Position { Value = new float3(t, 80, t) };

        us.damage = 0;
        us.dead = 0;
        us.health = 5;
        us.maxHealth = 5;
        us.score = 0;
        us.Safe = 3;

        em.SetComponentData(ship.Entity, p);
        em.SetComponentData(ship.Entity, us);
    }
}
