﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public enum TEAM : byte
{
    Null,
    PLAYER,
    RED,
    PURPLE,
}


[System.Serializable]
public struct GridTile : IComponentData
{
    public float3 position;

    public float HeightPowerValue;

    public byte emitter;

    public TEAM team;
}
public class CGridTile : ComponentDataWrapper<GridTile> { }

[InternalBufferCapacity(100)]
public struct GridPointsBuffer :IBufferElementData
{
    public GridTile tile;

    public static implicit operator GridTile(GridPointsBuffer e) { return e.tile; }
    public static implicit operator GridPointsBuffer(GridTile e) { return new GridPointsBuffer { tile = e }; }
}

[InternalBufferCapacity(100)]
public struct GridIntsBuffer : IBufferElementData
{
    public int ints;

    public static implicit operator int(GridIntsBuffer e) { return e.ints ; }
    public static implicit operator GridIntsBuffer(int e) { return new GridIntsBuffer { ints = e }; }
}
