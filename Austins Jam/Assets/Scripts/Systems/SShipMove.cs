﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

public class SShipMove : JobComponentSystem
{
    public static JobHandle handle;

    struct _Ship
    {
        public ComponentDataArray<UserShip> userShip;
        public ComponentDataArray<Position> pos;
    }

    [Inject] _Ship _ship;

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        int size = PersistantData.worldSize * PersistantData.worldSize;

        float LR = 1;
        float UD = 1;

        LR = Input.GetAxis("Horizontal");

        UD = Input.GetAxis("Vertical");

        Manage job = new Manage
        {
            userShip = _ship.userShip,
            pos = _ship.pos,

            size = PersistantData.worldSize,

            deltaTime = Time.deltaTime,

            LR =LR,
            UD=UD,
        };
        handle = job.Schedule(1, 1, inputDeps);

        return handle;
    }

    [BurstCompile]
    struct Manage : IJobParallelFor
    {
        public ComponentDataArray<UserShip> userShip;
        public ComponentDataArray<Position> pos;

        public int size;

        public float deltaTime;

        public float LR;
        public float UD;

        public void Execute(int index)
        {
            UserShip us = userShip[0];
            Position po = pos[0];

            if (us.dead==0)
            {
                po.Value.x += LR * 5*deltaTime*50;
                po.Value.z += UD * 5*deltaTime*50;
            }

            userShip[0] = us;
            pos[0] = po;
        }
    }
}
