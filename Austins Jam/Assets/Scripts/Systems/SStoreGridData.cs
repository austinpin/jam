﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

public class StoreBarrier : BarrierSystem { }

public class SStoreGridData: JobComponentSystem
{
    public static JobHandle handle;

    struct _Ints
    {
        public readonly int Length;
        public BufferArray<GridIntsBuffer> ints;
    }

    struct _Poss
    {
        public EntityArray self;
        [ReadOnly] public SharedComponentDataArray<PosArray> poss;
    }

    struct _Tiles
    {
        [ReadOnly] public BufferArray<GridPointsBuffer> points;
        public ComponentDataArray<GridData> gridData;
    }

    [Inject] _Tiles _tiles;
    [Inject] _Poss _poss;
    [Inject] _Ints _ints;

    [Inject] StoreBarrier barrier;

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        int size = PersistantData.worldSize * PersistantData.worldSize;
        int end = _tiles.gridData[0].div * _tiles.gridData[0].current;
        int start = _tiles.gridData[0].lastEnd;

        GridData d = _tiles.gridData[0];
        d.lastEnd = end;
        d.current++;
        if (d.current>=4)
        {
            d.current = 0;
            d.lastEnd = 0;
        }
        _tiles.gridData[0] = d;

        Work job = new Work
        {
            self = _poss.self,
            poss = _poss.poss,

            points = _tiles.points,

            ecb = barrier.CreateCommandBuffer().ToConcurrent(),

            size = size,
            end = end,
            start = start,
        };
        handle = job.Schedule(1, 1, inputDeps);

        return handle;
    }

    //[BurstCompile]
    struct Work : IJobParallelFor
    {
        public EntityArray self;
        [ReadOnly] public SharedComponentDataArray<PosArray> poss;

        [ReadOnly] public BufferArray<GridPointsBuffer> points;

        public EntityCommandBuffer.Concurrent ecb;

        public int size;
        public int end;
        public int start;

        public void Execute(int index)
        {
            Vector3[] newArray = poss[0].poss;
            for (int i=start;i<end;i++)
            {
                newArray[i] = points[0][i].tile.position;
                newArray[i] = new Vector3(points[0][i].tile.position.x,(points[0][i].tile.position.y + points[0][i].tile.HeightPowerValue), points[0][i].tile.position.z);
            }
            ecb.SetSharedComponent(0, self[0], new PosArray { poss = newArray });
        }
    }
}
