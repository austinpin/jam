﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuButtons : MonoBehaviour
{
    bool bInstructions = false;
    public GameObject buttons;
    public TextMeshProUGUI worldSizeText;

    // Start is called before the first frame update
    void Start()
    {
        buttons.SetActive(true);
        PersistantData.worldSize = 62;
    }

    // Update is called once per frame
    void Update()
    {
        worldSizeText.text = PersistantData.worldSize.ToString();
    }

    public void Play()
    {
        SceneManager.LoadScene("World", LoadSceneMode.Single);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void SetPersistentData(int size)
    {
        PersistantData.worldSize = size;
    }
}
