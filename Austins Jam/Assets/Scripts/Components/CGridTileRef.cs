﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct GridTileRef : ISharedComponentData
{
    public Mesh testMesh;
    public Material gridTileM;
}
public class CGridTileRef : SharedComponentDataWrapper<GridTileRef> { }