﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserWaveSet : IComponentData
{
    public float currentWave;

    public float2 blueWaveDist;
    public float2 redWaveDist;
}
public class CUserWaveSet : ComponentDataWrapper<UserWaveSet> { }
