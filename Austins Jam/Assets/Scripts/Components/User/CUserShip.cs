﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct UserShip : IComponentData
{
    public float Safe;

    public float speed;
    public float maxHealth;
    public float health;
    public float score;

    public byte dead;
    public byte damage;
}
public class CUserShip : ComponentDataWrapper<UserShip> { }