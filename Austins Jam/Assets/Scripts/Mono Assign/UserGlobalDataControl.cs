﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class UserGlobalDataControl : MonoBehaviour
{
    EntityManager em;
    Entity self;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        self = GetComponent<GameObjectEntity>().Entity;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetGlobalSpeed(float value)
    {
        if (value>4)
        {
            value = 4;
        }
        if (value < 0)
        {
            value = 0;
        }

        em.SetComponentData(self, new GD_Time { timeSpeed = value });
    }
}
