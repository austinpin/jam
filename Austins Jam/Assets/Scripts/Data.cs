﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;

public class Data : MonoBehaviour
{
    EntityManager em;

    public static EntityArchetype tile;
    public static EntityArchetype tileOwner;
    public static EntityArchetype tileInts;
    public static EntityArchetype tilePoses;

    // Start is called before the first frame update
    void Awake()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();

        tile = em.CreateArchetype(
            ComponentType.Create<GridTile>()
            );

        tileOwner = em.CreateArchetype(
    ComponentType.Create<GridPointsBuffer>(),
    ComponentType.Create<GridData>()
    );

    tileInts = em.CreateArchetype(
ComponentType.Create<GridIntsBuffer>()
    );

        tilePoses = em.CreateArchetype(
ComponentType.Create<PosArray>()
);

    }

// Update is called once per frame
void Update()
    {
        
    }
}
