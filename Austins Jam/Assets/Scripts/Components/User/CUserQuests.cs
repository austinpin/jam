﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UserQuests : IComponentData
{
    public byte placedStart;
}
public class CUserQuests : ComponentDataWrapper<UserQuests> { }
