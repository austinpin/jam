﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct GridData : IComponentData
{
    public int div;
    public int lastEnd;

    public int current;
    public int halfCurrent;
    
    public int halfDiv;
    public int lastHalfEnd;
}
public class CGridData : ComponentDataWrapper<GridData> { }