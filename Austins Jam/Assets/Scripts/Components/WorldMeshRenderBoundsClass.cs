﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Rendering;
using Unity.Entities;

[System.Serializable]
public class WorldMeshRenderBoundsClass : ComponentDataWrapper<WorldMeshRenderBounds> { }
