﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct UserData : IComponentData
{
    public float playerMoveDelay;
    public float counterx;
    public float countery;
}
public class CUserData : ComponentDataWrapper<UserData> { }