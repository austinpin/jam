﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct UIReferances : ISharedComponentData
{
    public TextMeshProUGUI selectedText;

    public EncylopediaButtons buttons;
}
public class CUIReferances : SharedComponentDataWrapper<UIReferances> { }