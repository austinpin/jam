﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;

public class SGridManage : JobComponentSystem
{
    public static JobHandle handle;

    struct _User
    {
        public ComponentDataArray<UserData> userData;
        
    }

    struct _Tiles
    {
        public BufferArray<GridPointsBuffer> points;
        public ComponentDataArray<GridData> gridData;
    }

    struct _Ship
    {
        public ComponentDataArray<UserShip> userShip;
        public ComponentDataArray<Position> pos;
    }

    [Inject] _Tiles _tiles;
    [Inject] _User _user;
    [Inject] _Ship _ship;

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        int size = PersistantData.worldSize * PersistantData.worldSize;
        int end = _tiles.gridData[0].halfDiv * _tiles.gridData[0].halfCurrent;
        int start = _tiles.gridData[0].lastHalfEnd;

        GridData d = _tiles.gridData[0];
        d.lastHalfEnd = end;
        d.halfCurrent++;
        if (d.halfCurrent >= 51)
        {
            d.halfCurrent = 0;
            d.lastHalfEnd = 0;
        }
        _tiles.gridData[0] = d;

        Manage job = new Manage
        {
            points = _tiles.points,

            userData = _user.userData,

            userShip = _ship.userShip,
            pos = _ship.pos,

            size = PersistantData.worldSize,

            deltaTime = Time.deltaTime,
        };
        handle = job.Schedule(1, 1, inputDeps);

        return handle;
    }

    [BurstCompile]
    struct Manage : IJobParallelFor
    {
        public BufferArray<GridPointsBuffer> points;

        public ComponentDataArray<UserData> userData;

        public ComponentDataArray<UserShip> userShip;
        public ComponentDataArray<Position> pos;

        public int size;

        public float deltaTime;

        public void Execute(int index)
        {
            UserData d = userData[0];
            d.playerMoveDelay += (deltaTime / 2);

            UserShip us = userShip[0];
            Position po = pos[0];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    GridTile t = points[0][i * size + j];

                    float ze = (float)(d.counterx + (t.position.x / 50)) / size * 4;
                    float fe = (float)(d.countery + (t.position.z / 50)) / size * 4;
                    float perl = Mathf.PerlinNoise(fe, ze) * 200;

                    float ze2 = (float)(-d.counterx + (t.position.x / 50)) / size * 6;
                    float fe2 = (float)(-d.countery + (t.position.z / 50)) / size * 6;
                    float p2 = Mathf.PerlinNoise(fe2, ze2) * 200;
                    //Debug.Log(perl);

                    float3 pos = t.position;
                    pos.y = perl +p2;
                    t.position = pos;

                    //buff.Add(new GridTile { position = pos, team = TEAM.Null });
                    //poses[i * size + j] = pos;

                    points[0].RemoveAt(i * size + j);
                    points[0].Insert(i * size + j, t);
                }
            }

            float ze3 = (float)(d.counterx + (po.Value.x / 50)) / size * 4;
            float fe3 = (float)(d.countery + (po.Value.z / 50)) / size * 4;
            float perl2 = Mathf.PerlinNoise(fe3, ze3) * 200;

            float ze4 = (float)(-d.counterx + (po.Value.x / 50)) / size * 6;
            float fe4 = (float)(-d.countery + (po.Value.z / 50)) / size * 6;
            float p4 = Mathf.PerlinNoise(fe4, ze4) * 200;

            //Debug.Log(p4+perl2);

            us.Safe -= deltaTime;
            if (us.Safe<=0)
            {
                if (p4 + perl2 < 170)
                {
                    if (us.health > -1)
                    {
                        us.damage = 1;
                        us.maxHealth -= deltaTime;
                        us.health -= deltaTime * 1.5f;
                    }
                }

                if (p4 + perl2 >= 170)
                {
                    us.damage = 0;
                    if (us.health > 0)
                    {
                        us.score += deltaTime;
                        us.health += deltaTime / 3;
                        if (us.health > us.maxHealth)
                        {
                            us.health = us.maxHealth;
                        }
                    }
                }
            }

            if (us.health<=0)
            {
                us.dead = 1;
            }

            po.Value.y = perl2 +p4;

            d.counterx += deltaTime * 5;
            d.countery += deltaTime * 5;
            if (d.counterx > 1000000000)
            {
                d.counterx = 0;
            }
            if (d.countery > 1000000000)
            {
                d.countery = 0;
            }

            userData[0] = d;
            userShip[0] = us;
            pos[0] = po;
        }
    }
}
