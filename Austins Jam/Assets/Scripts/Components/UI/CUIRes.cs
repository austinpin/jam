﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct UIRes : ISharedComponentData
{
    public TextMeshProUGUI money;
}
public class CUIRes : SharedComponentDataWrapper<UIRes> { }