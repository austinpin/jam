﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct GD_Time : IComponentData
{
    public float timeSpeed;
}
public class CGD_Time : ComponentDataWrapper<GD_Time> { }

