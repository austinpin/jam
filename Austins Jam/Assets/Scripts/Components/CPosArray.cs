﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public struct PosArray : ISharedComponentData
{
    public Vector3[] poss;
}
public class CPosArray : SharedComponentDataWrapper<PosArray> { }