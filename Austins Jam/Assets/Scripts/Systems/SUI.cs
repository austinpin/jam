﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;
using TMPro;

//[UpdateAfter(typeof(SPlayerSubAction))]
public class SUI : ComponentSystem
{

    struct _PlayerSelect
    {
        public readonly int Length;
        public EntityArray self;
        [ReadOnly] public SharedComponentDataArray<UIReferances> uiReferances;
        [ReadOnly] public SharedComponentDataArray<UIRes> uiRes;
    }

    struct _User
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<UserQuests> quests;
    }

    [Inject] _PlayerSelect _player;
    [Inject] _User _user;

    protected override void OnUpdate()
    {

    }
}
