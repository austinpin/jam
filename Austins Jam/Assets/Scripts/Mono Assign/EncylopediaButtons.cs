﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class EncylopediaButtons : MonoBehaviour
{
    public GameObject self;

    public GameObject EncylopediaG;
    public GameObject TutorialHelpG;
    public GameObject GeneralG;

    public GameObject StartingG;
    public GameObject FirstResG;
    public GameObject MiningG;
    public GameObject GeneratorG;
    public GameObject BaseG;

    public GameObject PlacingG;
    public GameObject BlueprintsG;
    public GameObject ZonesG;
    public GameObject ClickablesG;
    public GameObject ResourcesG;

    public GameObject winG;

    EntityManager em;
    public GameObjectEntity quests;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        TurnOn(1);
        Encylopedia(1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenHelp()
    {
        UserQuests uq = em.GetComponentData<UserQuests>(quests.Entity);

        TurnOn(1);
    }

    public void TurnOn(int on)
    {
        if (on==1)
        {
            self.SetActive(true);
        }
        else if(on==2)
        {
            self.SetActive(false);
        }
        
    }

    void AllOff()
    {
        EncylopediaG.SetActive(false);
        TutorialHelpG.SetActive(false);
        GeneralG.SetActive(false);

        StartingG.SetActive(false);
        FirstResG.SetActive(false);
        MiningG.SetActive(false);
        GeneratorG.SetActive(false);
        BaseG.SetActive(false);

        PlacingG.SetActive(false);
        BlueprintsG.SetActive(false);
        ZonesG.SetActive(false);
        ClickablesG.SetActive(false);
        ResourcesG.SetActive(false);

        winG.SetActive(false);
    }

    public void Encylopedia(int index)
    {
        AllOff();
        EncylopediaG.SetActive(true);
    }

    public void TutorialHelp(int index)
    {
        AllOff();
        TutorialHelpG.SetActive(true);
        if (index == 1)
        {
            StartingG.SetActive(true);
        }
        if (index == 2)
        {
            FirstResG.SetActive(true);
        }
        if (index == 3)
        {
            MiningG.SetActive(true);
        }
        if (index == 4)
        {
            GeneratorG.SetActive(true);
        }
        if (index == 5)
        {
            BaseG.SetActive(true);
        }
    }

    public void General(int index)
    {
        AllOff();
        GeneralG.SetActive(true);
        if (index == 1)
        {
            PlacingG.SetActive(true);
        }
        if (index == 2)
        {
            BlueprintsG.SetActive(true);
        }
        if (index == 3)
        {
            ZonesG.SetActive(true);
        }
        if (index == 4)
        {
            ClickablesG.SetActive(true);
        }
        if (index == 5)
        {
            ResourcesG.SetActive(true);
        }
    }

    public void Win()
    {
        TurnOn(1);
        AllOff();
        winG.SetActive(true);
    }
}
