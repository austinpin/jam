﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;

//[UpdateAfter(typeof(SPlayerSubAction))]
public class SDrawGrid : ComponentSystem
{

    struct _Tiles
    {
        [ReadOnly] public BufferArray<GridPointsBuffer> points;
    }

    struct _Refs
    {
        [ReadOnly] public SharedComponentDataArray<GridTileRef> refs;
    }

    struct _Poss
    {
        [ReadOnly] public SharedComponentDataArray<PosArray> poss;
    }

    [Inject] _Tiles _tiles;
    [Inject] _Refs _refs;
    [Inject] _Poss _poss;

    private MaterialPropertyBlock block;
    private int colorID;
    Mesh m1;
    bool ini=true;

    protected override void OnCreateManager()
    {
        block = new MaterialPropertyBlock();
        colorID = Shader.PropertyToID("Color_AEB7214D");

        m1 = new Mesh();
    }

    protected override void OnUpdate()
    {
        if (ini)
        {
            ini = false;

            int worldSize = PersistantData.worldSize;

            int total = worldSize * worldSize;
            int total2 = (worldSize - 1) * (worldSize - 1);
            Vector3[] verts = new Vector3[total];

            int[] tris = new int[total2 * 6];
            Vector2[] uvs = new Vector2[total];

            for (int i = 0; i < total; i++)
            {
                verts[i] = _tiles.points[0][i].tile.position;
            }

            m1.vertices = verts;

            for (int i = 0; i < worldSize; i++)
            {
                for (int j = 0; j < worldSize; j++)
                {
                    int index = i * worldSize + j;
                    //Vector3 newPos = new Vector3(_tiles.points[0][index].tile.position.x, _tiles.points[0][index].tile.position.y, _tiles.points[0][index].tile.position.z);
                    //verts[index] = new Vector3(_tiles.points[0][index].tile.position.x, _tiles.points[0][index].tile.position.y, _tiles.points[0][index].tile.position.z);
                    uvs[index] = new Vector2(i, j);
                }
            }

            int iterator = 0;

            for (int i = 0; i < worldSize - 1; i++)
            {
                for (int j = 0; j < worldSize - 1; j++)
                {
                    tris[((iterator) * 6) + 0] = i * worldSize + j;
                    tris[((iterator) * 6) + 1] = (i + 1) * worldSize + j;
                    tris[((iterator) * 6) + 2] = (i + 1) * worldSize + (j + 1);
                    tris[((iterator) * 6) + 3] = (i + 1) * worldSize + (j + 1);
                    tris[((iterator) * 6) + 4] = i * worldSize + (j + 1);
                    tris[((iterator) * 6) + 5] = i * worldSize + j;

                    iterator++;
                }
            }

            m1.triangles = tris;
            m1.uv = uvs;
        }

        /*
        for (int i = 0; i < total; i++)
        {
            //Vector3 newPos = new Vector3(_tiles.points[0][index].tile.position.x, _tiles.points[0][index].tile.position.y, _tiles.points[0][index].tile.position.z);
            verts[i] = _tiles.points[0][i].tile.position;
            //uvs[index] = new Vector2(i, j);
        }
        */

        m1.vertices = _poss.poss[0].poss;

        m1.RecalculateNormals();
        Graphics.DrawMesh(m1, new Vector3(0, 0, 0), Quaternion.identity, _refs.refs[0].gridTileM, 0);
    }
}
